// Made with Blockbench 4.2.4
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports

public static class Modelskeleton_gun_model extends EntityModel<Entity> {
	private final ModelRenderer head;
	private final ModelRenderer body;
	private final ModelRenderer right_arm;
	private final ModelRenderer left_arm;
	private final ModelRenderer right_leg;
	private final ModelRenderer left_leg;
	private final ModelRenderer bone;
	private final ModelRenderer group2;
	private final ModelRenderer cube_r1;
	private final ModelRenderer group3;
	private final ModelRenderer group4;
	private final ModelRenderer cube_r2;
	private final ModelRenderer cube_r3;
	private final ModelRenderer cube_r4;
	private final ModelRenderer cube_r5;
	private final ModelRenderer cube_r6;
	private final ModelRenderer cube_r7;
	private final ModelRenderer cube_r8;
	private final ModelRenderer group5;
	private final ModelRenderer group;
	private final ModelRenderer cube_r9;
	private final ModelRenderer cube_r10;
	private final ModelRenderer cube_r11;
	private final ModelRenderer cube_r12;
	private final ModelRenderer cube_r13;
	private final ModelRenderer cube_r14;
	private final ModelRenderer cube_r15;
	private final ModelRenderer cube_r16;
	private final ModelRenderer cube_r17;
	private final ModelRenderer cube_r18;

	public Modelskeleton_gun_model() {
		textureWidth = 64;
		textureHeight = 64;

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, 0.0F, 0.0F);
		head.setTextureOffset(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 24.0F, 0.0F);
		body.setTextureOffset(16, 16).addBox(-4.0F, -24.0F, -2.0F, 8.0F, 12.0F, 4.0F, 0.0F, false);

		right_arm = new ModelRenderer(this);
		right_arm.setRotationPoint(-5.0F, 2.0F, 0.0F);
		right_arm.setTextureOffset(0, 16).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);

		left_arm = new ModelRenderer(this);
		left_arm.setRotationPoint(5.0F, 2.0F, 0.0F);
		left_arm.setTextureOffset(0, 16).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 12.0F, 2.0F, 0.0F, true);

		right_leg = new ModelRenderer(this);
		right_leg.setRotationPoint(-1.9F, 12.0F, 0.0F);
		right_leg.setTextureOffset(40, 16).addBox(-1.1F, 0.0F, -1.0F, 2.0F, 12.0F, 2.0F, 0.0F, false);

		left_leg = new ModelRenderer(this);
		left_leg.setRotationPoint(1.9F, 12.0F, 0.0F);
		left_leg.setTextureOffset(40, 16).addBox(-0.9F, 0.0F, -1.0F, 2.0F, 12.0F, 2.0F, 0.0F, true);

		bone = new ModelRenderer(this);
		bone.setRotationPoint(-14.0F, 10.0F, 0.0F);
		setRotationAngle(bone, 0.0F, 3.1416F, 0.0F);

		group2 = new ModelRenderer(this);
		group2.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone.addChild(group2);
		group2.setTextureOffset(3, 33).addBox(-9.0F, -10.5F, 10.025F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		group2.setTextureOffset(3, 33).addBox(-9.0F, -10.5F, 1.3F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		group2.setTextureOffset(3, 32).addBox(-8.7F, -10.2917F, 8.3002F, 0.0F, 2.0F, 0.0F, 0.0F, false);
		group2.setTextureOffset(3, 32).addBox(-8.7F, -10.2917F, 5.1002F, 0.0F, 2.0F, 0.0F, 0.0F, false);
		group2.setTextureOffset(3, 39).addBox(-8.65F, -10.25F, 2.625F, 0.0F, 1.0F, 7.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(-8.2F, -9.7F, 11.1875F);
		group2.addChild(cube_r1);
		setRotationAngle(cube_r1, 0.0F, 0.0F, 0.7854F);
		cube_r1.setTextureOffset(3, 33).addBox(-0.4F, -0.4F, -10.0625F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r1.setTextureOffset(3, 33).addBox(-0.4F, -0.4F, -0.8625F, 0.0F, 0.0F, 1.0F, 0.0F, false);

		group3 = new ModelRenderer(this);
		group3.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone.addChild(group3);
		group3.setTextureOffset(3, 55).addBox(-8.5F, -8.05F, 18.175F, 0.0F, 0.0F, 8.0F, 0.0F, false);
		group3.setTextureOffset(3, 48).addBox(-8.5F, -8.925F, 24.975F, 0.0F, 0.0F, 1.0F, 0.0F, false);
		group3.setTextureOffset(3, 48).addBox(-8.5F, -8.175F, 20.025F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		group3.setTextureOffset(3, 49).addBox(-8.5F, -7.575F, 21.4F, 0.0F, 0.0F, 2.0F, 0.0F, false);
		group3.setTextureOffset(3, 49).addBox(-8.3F, -7.05F, 23.425F, 0.0F, 0.0F, 2.0F, 0.0F, false);

		group4 = new ModelRenderer(this);
		group4.setRotationPoint(0.025F, 0.0759F, 0.0061F);
		bone.addChild(group4);
		group4.setTextureOffset(35, 59).addBox(-8.525F, -5.5F, 2.6F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		group4.setTextureOffset(35, 59).addBox(-8.525F, -6.3816F, 3.6566F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		group4.setTextureOffset(35, 59).addBox(-8.525F, -6.3816F, 2.2066F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		group4.setTextureOffset(35, 59).addBox(-8.525F, -6.3816F, 2.0566F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		group4.setTextureOffset(35, 59).addBox(-8.525F, -6.3775F, 2.8235F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		group4.setTextureOffset(35, 59).addBox(-8.525F, -6.3775F, 2.719F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		group4.setTextureOffset(35, 59).addBox(-8.525F, -6.3775F, 2.7735F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r2 = new ModelRenderer(this);
		cube_r2.setRotationPoint(-8.025F, -5.6063F, 2.4497F);
		group4.addChild(cube_r2);
		setRotationAngle(cube_r2, 0.7854F, 0.0F, 0.0F);
		cube_r2.setTextureOffset(35, 59).addBox(-0.5F, -0.3375F, -0.075F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r3 = new ModelRenderer(this);
		cube_r3.setRotationPoint(-8.025F, -5.7525F, 2.811F);
		group4.addChild(cube_r3);
		setRotationAngle(cube_r3, 0.3927F, 0.0F, 0.0F);
		cube_r3.setTextureOffset(35, 59).addBox(-0.5F, -0.15F, -0.0375F, 0.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r3.setTextureOffset(35, 59).addBox(-0.5F, -0.075F, -0.0375F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r4 = new ModelRenderer(this);
		cube_r4.setRotationPoint(-8.025F, -5.7525F, 2.811F);
		group4.addChild(cube_r4);
		setRotationAngle(cube_r4, -0.3927F, 0.0F, 0.0F);
		cube_r4.setTextureOffset(35, 59).addBox(-0.5F, -0.55F, -0.1625F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r5 = new ModelRenderer(this);
		cube_r5.setRotationPoint(-8.025F, -6.1814F, 2.8534F);
		group4.addChild(cube_r5);
		setRotationAngle(cube_r5, -0.7854F, 0.0F, 0.0F);
		cube_r5.setTextureOffset(35, 59).addBox(-0.5F, -0.3125F, -0.0875F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r6 = new ModelRenderer(this);
		cube_r6.setRotationPoint(-8.025F, -5.8191F, 2.8316F);
		group4.addChild(cube_r6);
		setRotationAngle(cube_r6, 0.7854F, 0.0F, 0.0F);
		cube_r6.setTextureOffset(35, 59).addBox(-0.5F, 0.0875F, -0.125F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r7 = new ModelRenderer(this);
		cube_r7.setRotationPoint(-8.025F, -6.0552F, 2.2384F);
		group4.addChild(cube_r7);
		setRotationAngle(cube_r7, 0.3927F, 0.0F, 0.0F);
		cube_r7.setTextureOffset(35, 59).addBox(-0.5F, -0.3375F, -0.175F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r8 = new ModelRenderer(this);
		cube_r8.setRotationPoint(-8.025F, -5.5798F, 3.5237F);
		group4.addChild(cube_r8);
		setRotationAngle(cube_r8, 0.7854F, 0.0F, 0.0F);
		cube_r8.setTextureOffset(35, 59).addBox(-0.5F, -0.075F, -0.25F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		group5 = new ModelRenderer(this);
		group5.setRotationPoint(0.025F, 0.0759F, 0.0061F);
		bone.addChild(group5);
		group5.setTextureOffset(29, 55).addBox(-8.725F, -8.3F, 2.575F, 0.0F, 0.0F, 9.0F, 0.0F, false);
		group5.setTextureOffset(35, 59).addBox(-8.725F, -8.3F, 2.575F, 0.0F, 0.0F, 1.0F, 0.0F, false);
		group5.setTextureOffset(34, 57).addBox(-8.375F, -8.0F, 2.4F, 0.0F, 0.0F, 0.0F, 0.0F, false);

		group = new ModelRenderer(this);
		group.setRotationPoint(-8.0F, -2.5645F, -7.0195F);
		bone.addChild(group);
		group.setTextureOffset(30, 38).addBox(-1.0F, -4.6012F, -0.3331F, 1.0F, 0.0F, 6.0F, 0.0F, false);
		group.setTextureOffset(30, 34).addBox(-1.0F, -4.9594F, 8.6668F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		group.setTextureOffset(27, 36).addBox(-1.0F, -5.1845F, 11.0007F, 1.0F, 1.0F, 16.0F, 0.0F, false);
		group.setTextureOffset(30, 37).addBox(-1.0F, -5.7345F, 19.2257F, 1.0F, 1.0F, 5.0F, 0.0F, false);
		group.setTextureOffset(30, 36).addBox(-1.0F, -4.8845F, 8.9257F, 1.0F, 1.0F, 4.0F, 0.0F, false);
		group.setTextureOffset(30, 38).addBox(-1.0F, -5.0345F, 11.0007F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		group.setTextureOffset(30, 36).addBox(-1.0F, -4.3494F, 0.2838F, 1.0F, 2.0F, 4.0F, 0.0F, false);
		group.setTextureOffset(30, 33).addBox(-1.0F, -3.9262F, -0.3331F, 1.0F, 3.0F, 1.0F, 0.0F, false);
		group.setTextureOffset(30, 33).addBox(-1.0F, -4.6012F, -0.4581F, 1.0F, 4.0F, 0.0F, 0.0F, false);

		cube_r9 = new ModelRenderer(this);
		cube_r9.setRotationPoint(0.0F, 0.0F, 0.0F);
		group.addChild(cube_r9);
		setRotationAngle(cube_r9, -0.3927F, 0.0F, 0.0F);
		cube_r9.setTextureOffset(30, 32).addBox(-1.0F, -0.475F, -0.45F, 1.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r10 = new ModelRenderer(this);
		cube_r10.setRotationPoint(0.0F, -3.4283F, 7.5507F);
		group.addChild(cube_r10);
		setRotationAngle(cube_r10, 0.7854F, 0.0F, 0.0F);
		cube_r10.setTextureOffset(30, 33).addBox(-1.0F, -0.2355F, -1.2411F, 1.0F, 0.0F, 1.0F, 0.0F, false);
		cube_r10.setTextureOffset(30, 33).addBox(-1.0F, -0.2355F, -1.2411F, 1.0F, 0.0F, 1.0F, 0.0F, false);

		cube_r11 = new ModelRenderer(this);
		cube_r11.setRotationPoint(0.0F, -4.1141F, 6.4617F);
		group.addChild(cube_r11);
		setRotationAngle(cube_r11, 0.3927F, 0.0F, 0.0F);
		cube_r11.setTextureOffset(30, 41).addBox(-1.0F, 0.0629F, -6.6467F, 1.0F, 1.0F, 9.0F, 0.0F, false);

		cube_r12 = new ModelRenderer(this);
		cube_r12.setRotationPoint(0.0F, -4.1238F, 11.197F);
		group.addChild(cube_r12);
		setRotationAngle(cube_r12, 0.3927F, 0.0F, 0.0F);
		cube_r12.setTextureOffset(30, 38).addBox(-1.0F, -2.3217F, -10.7732F, 1.0F, 0.0F, 6.0F, 0.0F, false);

		cube_r13 = new ModelRenderer(this);
		cube_r13.setRotationPoint(0.0F, -4.3059F, 11.3148F);
		group.addChild(cube_r13);
		setRotationAngle(cube_r13, 0.3927F, 0.0F, 0.0F);
		cube_r13.setTextureOffset(30, 32).addBox(-1.0F, 2.0463F, 5.1383F, 1.0F, 0.0F, 0.0F, 0.0F, false);
		cube_r13.setTextureOffset(30, 32).addBox(-1.0F, 0.8713F, 1.9859F, 1.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r14 = new ModelRenderer(this);
		cube_r14.setRotationPoint(0.0F, -5.1309F, 21.7523F);
		group.addChild(cube_r14);
		setRotationAngle(cube_r14, -0.3927F, 0.0F, 0.0F);
		cube_r14.setTextureOffset(30, 33).addBox(-1.0F, -1.6777F, 2.4729F, 1.0F, 0.0F, 1.0F, 0.0F, false);

		cube_r15 = new ModelRenderer(this);
		cube_r15.setRotationPoint(0.0F, -4.5067F, 5.942F);
		group.addChild(cube_r15);
		setRotationAngle(cube_r15, -0.3927F, 0.0F, 0.0F);
		cube_r15.setTextureOffset(30, 32).addBox(-1.0F, -0.183F, 0.1946F, 1.0F, 0.0F, 0.0F, 0.0F, false);

		cube_r16 = new ModelRenderer(this);
		cube_r16.setRotationPoint(0.0F, -3.0924F, 7.3562F);
		group.addChild(cube_r16);
		setRotationAngle(cube_r16, 0.3927F, 0.0F, 0.0F);
		cube_r16.setTextureOffset(30, 33).addBox(-1.0F, -0.5605F, 0.5339F, 1.0F, 0.0F, 1.0F, 0.0F, false);

		cube_r17 = new ModelRenderer(this);
		cube_r17.setRotationPoint(0.0F, -2.8637F, 6.5294F);
		group.addChild(cube_r17);
		setRotationAngle(cube_r17, -0.3927F, 0.0F, 0.0F);
		cube_r17.setTextureOffset(30, 33).addBox(-1.0F, -0.3125F, -0.5625F, 1.0F, 0.0F, 1.0F, 0.0F, false);

		cube_r18 = new ModelRenderer(this);
		cube_r18.setRotationPoint(0.0F, -1.8512F, 2.8169F);
		group.addChild(cube_r18);
		setRotationAngle(cube_r18, 0.3927F, 0.0F, 0.0F);
		cube_r18.setTextureOffset(30, 38).addBox(-1.0F, -0.025F, -2.975F, 1.0F, 0.0F, 6.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		head.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		body.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		right_arm.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		left_arm.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		right_leg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		left_leg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		bone.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
	}
}