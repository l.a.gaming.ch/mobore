// Made with Blockbench 4.2.4
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports

public static class Modellance extends EntityModel<Entity> {
	private final ModelRenderer group;

	public Modellance() {
		textureWidth = 64;
		textureHeight = 64;

		group = new ModelRenderer(this);
		group.setRotationPoint(-0.5F, 7.7279F, 0.1765F);
		setRotationAngle(group, -1.5708F, 0.0F, 0.0F);
		group.setTextureOffset(58, 44).addBox(-0.5F, -0.5F, -9.3125F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		group.setTextureOffset(56, 44).addBox(-1.0F, -1.0F, -8.3125F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		group.setTextureOffset(54, 43).addBox(-1.5F, -1.5F, -6.3125F, 3.0F, 3.0F, 2.0F, 0.0F, false);
		group.setTextureOffset(50, 41).addBox(-2.0F, -2.0F, -4.3125F, 4.0F, 4.0F, 3.0F, 0.0F, false);
		group.setTextureOffset(48, 40).addBox(-2.5F, -2.5F, -1.3125F, 5.0F, 5.0F, 3.0F, 0.0F, false);
		group.setTextureOffset(20, 42).addBox(-0.5F, -0.5F, 5.6875F, 1.0F, 1.0F, 21.0F, 0.0F, false);
		group.setTextureOffset(51, 34).addBox(-1.5F, -1.5F, 1.6875F, 3.0F, 3.0F, 3.0F, 0.0F, false);
		group.setTextureOffset(26, 10).addBox(-1.0F, -1.0F, 3.6875F, 2.0F, 2.0F, 2.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		group.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
	}
}