// Made with Blockbench 4.2.4
// Exported for Minecraft version 1.15 - 1.16 with MCP mappings
// Paste this class into your mod and generate all required imports

public static class Modelbee_model extends EntityModel<Entity> {
	private final ModelRenderer head;
	private final ModelRenderer body;
	private final ModelRenderer right_arm;
	private final ModelRenderer cube_r1;
	private final ModelRenderer left_arm;
	private final ModelRenderer right_leg;
	private final ModelRenderer left_leg;

	public Modelbee_model() {
		textureWidth = 64;
		textureHeight = 64;

		head = new ModelRenderer(this);
		head.setRotationPoint(0.0F, 0.0F, 0.0F);
		head.setTextureOffset(2, 1).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, 0.0F, false);
		head.setTextureOffset(2, 3).addBox(-3.0F, -8.0F, -7.0F, 1.0F, 3.0F, 3.0F, 0.0F, false);
		head.setTextureOffset(2, 0).addBox(2.0F, -8.0F, -7.0F, 1.0F, 3.0F, 3.0F, 0.0F, false);

		body = new ModelRenderer(this);
		body.setRotationPoint(0.0F, 24.0F, 0.0F);
		body.setTextureOffset(40, 0).addBox(-4.0F, -24.0F, -2.0F, 8.0F, 12.0F, 4.0F, 0.0F, false);
		body.setTextureOffset(13, 24).addBox(1.5F, -23.0F, 2.0F, 9.0F, 6.0F, 0.0F, 0.0F, true);
		body.setTextureOffset(13, 24).addBox(-10.5F, -23.0F, 2.0F, 9.0F, 6.0F, 0.0F, 0.0F, false);

		right_arm = new ModelRenderer(this);
		right_arm.setRotationPoint(-5.0F, 2.0F, 0.0F);
		setRotationAngle(right_arm, -2.8798F, 0.0F, 0.0F);
		right_arm.setTextureOffset(0, 32).addBox(-3.0F, -2.0F, -2.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		cube_r1 = new ModelRenderer(this);
		cube_r1.setRotationPoint(5.0F, 22.0F, 0.0F);
		right_arm.addChild(cube_r1);
		setRotationAngle(cube_r1, 2.7925F, 0.0F, 0.0F);
		cube_r1.setTextureOffset(26, 10).addBox(-7.5F, 12.5F, -6.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		cube_r1.setTextureOffset(51, 34).addBox(-8.0F, 12.0F, -8.0F, 3.0F, 3.0F, 3.0F, 0.0F, false);
		cube_r1.setTextureOffset(20, 42).addBox(-7.0F, 13.0F, -4.0F, 1.0F, 1.0F, 21.0F, 0.0F, false);
		cube_r1.setTextureOffset(48, 40).addBox(-9.0F, 11.0F, -11.0F, 5.0F, 5.0F, 3.0F, 0.0F, false);
		cube_r1.setTextureOffset(50, 41).addBox(-8.5F, 11.5F, -14.0F, 4.0F, 4.0F, 3.0F, 0.0F, false);
		cube_r1.setTextureOffset(54, 43).addBox(-8.0F, 12.0F, -16.0F, 3.0F, 3.0F, 2.0F, 0.0F, false);
		cube_r1.setTextureOffset(56, 44).addBox(-7.5F, 12.5F, -18.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		cube_r1.setTextureOffset(58, 44).addBox(-7.0F, 13.0F, -19.0F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		left_arm = new ModelRenderer(this);
		left_arm.setRotationPoint(5.0F, 2.0F, 0.0F);
		left_arm.setTextureOffset(0, 32).addBox(-1.0F, -2.0F, -2.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		right_leg = new ModelRenderer(this);
		right_leg.setRotationPoint(-1.9F, 12.0F, 0.0F);
		right_leg.setTextureOffset(23, 32).addBox(-2.1F, 0.0F, -2.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);

		left_leg = new ModelRenderer(this);
		left_leg.setRotationPoint(1.9F, 12.0F, 0.0F);
		left_leg.setTextureOffset(23, 32).addBox(-1.9F, 0.0F, -2.0F, 4.0F, 12.0F, 4.0F, 0.0F, false);
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red,
			float green, float blue, float alpha) {
		head.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		body.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		right_arm.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		left_arm.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		right_leg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		left_leg.render(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5, Entity e) {
		this.head.rotateAngleY = f3 / (180F / (float) Math.PI);
		this.head.rotateAngleX = f4 / (180F / (float) Math.PI);
		this.left_leg.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.left_arm.rotateAngleX = MathHelper.cos(f * 1.0F) * -1.0F * f1;
		this.right_leg.rotateAngleX = MathHelper.cos(f * 1.0F) * 1.0F * f1;
	}
}